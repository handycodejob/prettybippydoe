#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Sydney Henry'
SITENAME = 'Pretty Bippy Doe Podcast'
SITEURL = 'https://handycodejob.gitlab.io/prettybippydoe'
THEME = 'themes/blueidea'

PATH = 'content'

TIMEZONE = 'America/Phoenix'

DEFAULT_LANG = 'en'

STATIC_PATHS = (
    'static',
)

EXTRA_PATH_METADATA = {
    'static/robots.txt': {'path': 'robots.txt'},
    'static/favicon.ico': {'path': 'favicon.ico'},
}


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# links
LINKS = (
    ("Python.org", 'http://python.org'),
    ("Site's Repo", 'https://gitlab.com/handycodejob/prettybippydoe'),
)

# Social widget
SOCIAL = (
    ('gitlab', 'https://gitlab.com/handycodejob'),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True


# these files are ignored by the processor, it will however still allow
# the rst files be used with `.. include::`
IGNORE_FILES = (
    '__pycache__',
    'links.rst',
    'README.rst',
)

PAGES_SORT_ATTRIBUTE = 'order'
