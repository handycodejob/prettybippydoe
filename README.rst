
**********************************
HandyCodeJob's Pelican Boilerplate
**********************************

`Website <https://prettybippydoe.com/>`_

Cloneing repo
#############

We use submodules for themes, so be sure to use ``--recursive`` when you clone, like this:

.. code-block:: bash

    git clone git@gitlab.com:prettybippydoe/pelican-boilerplate.git --recursive prettybippydoe 
    git clone https://gitlab.com/prettybippydoe/pelican-boilerplate.git --recursive prettybippydoe


Using
#####

After you clone, make your environment, we use pipenv

.. code-block:: bash

    cd prettybippydoe
    pipenv install
    make devserver

That will watch the themes and content folders, and will update the build folder on edits and opens a server at `<http://localhost:8000>`_
Make sure that when you are done you use ``make stopserver`` to kill the processes that are running.

Changing Theme
##############

If it is a new theme, fork the theme to have it on gitlab.

TODO: add when we change theme

Then in ``pelicanconf.py`` change the theme.

Renew/Generate SSL Cert With Let's Encrypt
##########################################

This section is a bit terse, see `<https://fedoramagazine.org/gitlab-pelican-lets-encrypt-secure-blog/>`_ for more info.

.. code-block:: bash

        pip install certbot
        certbot certonly -a manual -d prettybippydoe.com -d www.prettybippydoe.com --config-dir ~/letsencrypt/config --work-dir ~/letsencrypt/work --logs-dir ~/letsencrypt/logs

That will start the process for both domains.
You will be asked some questions and notified that your IP will be logged.
Eventually you will get a message like this:

.. code-block:: bash

        Create a file containing just this data:
        S4SuxzAKzQr2tG3-V9IupxcelWcqd-CcbpDp9hlwNXA.dkJEs-bJbs7p-frPHaI1K2v-0nKvq_FzyWgWWs6jK4s
        And make it available on your web server at this URL:
        http://prettybippydoe.com/.well-known/acme-challenge/S4SuxzAKzQr2tG3-V9IupxcelWcqd-CcbpDp9hlwNXA

In a different terminal:

.. code-block:: bash

        mkdir -p .well-known/acme-challenge/
        echo S4SuxzAKzQr2tG3-V9IupxcelWcqd-CcbpDp9hlwNXA.dkJEs-bJbs7p-frPHaI1K2v-0nKvq_FzyWgWWs6jK4s > .well-known/acme-challenge/S4SuxzAKzQr2tG3-V9IupxcelWcqd-CcbpDp9hlwNXA

Then add the following to the script section of ``.gitlab-ci.yml``:

.. code-block:: yml
        
              script:
                - mkdir -p public/.well-known/acme-challenge/
                - mv .well-known/acme-challenge/*  public/.well-known/acme-challenge/

Do a git add, commit, and push to fire off the gitlab CI. 
Once the site is updated, use curl to check

.. code-block:: bash

        curl http://prettybippydoe.com/.well-known/acme-challenge/S4SuxzAKzQr2tG3-V9IupxcelWcqd-CcbpDp9hlwNXA

If curl just returns 

.. code-block:: bash 
        S4SuxzAKzQr2tG3-V9IupxcelWcqd-CcbpDp9hlwNXA.dkJEs-bJbs7p-frPHaI1K2v-0nKvq_FzyWgWWs6jK4s

Then go back to the certbot terminal and hit enter.
You will then need to re-run the echo command with the new strings and do another git add, comit, and push.
Hit enter again on the cerbot and copy and past the ``.pem`` and ``.key`` files (they will be the same for both domains) i.e. `<https://gitlab.com/HandyCodeJob/pelican-boilerplate/pages/domains/prettybippydoe.com/edit>`_
