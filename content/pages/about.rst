About Us
########

:date: 2018-03-14 17:00
:modified: 2018-03-15 14:30
:tags: about
:category: Pages
:slug: about
:authors: Sydney Henry
:summary: About Us
:order: 2


It is a family affair!

####
Cast
####

*************************
Kelly Henry (Aunt Pretty)
*************************

.. image:: {filename}/static/images/portrait_placeholder.png
    :align: left
    :alt: Picture of Kelly Henry

Kelly Henry is the mother of two, Mike and Sydney, and is married to her Husband of 25+ years.
She has 6 chickens and has recently gotten into quilting.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

*****************************
Stephanie Wright (Aunt Bippy)
*****************************

.. image:: {filename}/static/images/portrait_placeholder.png
    :align: left
    :alt: Picture of Stephanie Wright

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

**********************
Tracy Montgomery (Doe)
**********************

.. image:: {filename}/static/images/portrait_placeholder.png
    :align: left
    :alt: Picture of Tracy Montgomery

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.


######
Guests
######

*************************
Susan Montgomery (Mother)
*************************

.. image:: {filename}/static/images/portrait_placeholder.png
    :align: left
    :alt: Picture of Susan Montgomery

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

#######
Support
#######

************************
Sydney Henry (developer)
************************

.. image:: {filename}/static/images/sydney_henry.jpg
    :align: left
    :alt: Picture of Sydney Henry

Sydney is in  charge of all things technical, be that audio or website.
For work, she is a web developer through contracting with `HandyCodeJob`_
She enjoys to take care of dogs, work on side projects, and play computer games.

In her free time, she is normally watching someone streaming on Twitch or catching up on some YouTube videos.
While she rarely watches TV, if left alone with one, it will surely be on Law & Order.

***********************************
Emily Wright (Soical Media Manager)
***********************************

.. image:: {filename}/static/images/portrait_placeholder.png
    :align: left
    :alt: Picture of Emily Wright

Emily is currently a Junior at Perry High School.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

.. include:: ../links.rst
