Home
####

:date: 2018-03-17 14:50
:tags: home
:category: Blog
:url:
:save_as: index.html
:authors: Sydney Henry
:summary: Home
:order: 1

Welcome to Pretty Bippy Doe. We do bi weekly podcasts about raising kids and Lorem ipsum dolor sit amet.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

.. include:: ../links.rst
